-- Структура данных 
data Term = Variable String     -- Какая-либо перемменная
          | Lambda String Term  -- Лямбда от переменной (возвращает терм)
          | Apply Term Term     -- Выполнение терма


-- Класс Show используется для вывода терма (принимает тип  Term)
instance Show Term where
    show (Variable x) = x
    show (Apply term1 term2) = (show term1) ++ " " ++ (show term2)
    show (Lambda var res) = "(\\" ++ var ++ "." ++ (show res) ++ ")" 

-- Получение уникальной строки, которой нет в списке
renameVariable :: [String] -> String -> Int -> String
renameVariable lst x n | elem (x ++ (show n)) lst = renameVariable lst x (n+1)
                       | otherwise = x ++ (show n)

-- Переименовывание всех переменных, имена которых совпадают с теми, что есть в списке
renameFreeVariables :: [String] -> Term -> Term
renameFreeVariables lst (Lambda x term) | elem x lst = Lambda x (renameFreeVariables (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVariables lst term)
renameFreeVariables lst (Apply t1 t2) = Apply (renameFreeVariables lst t1) (renameFreeVariables lst t2)
renameFreeVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                                     | otherwise = Variable x


-- Функция, заменяющая данную переменную на нужный терм 
setVariable :: [String] -> Term -> String -> Term -> Term
setVariable lst (Lambda x term) needTerm var = if needTerm == x then (Lambda x term) else (Lambda x (setVariable (x:lst) term needTerm var)) 
setVariable lst (Apply term1 term2) needTerm var = Apply (setVariable lst term1 needTerm var) (setVariable lst term2 needTerm var)
setVariable lst (Variable x) needTerm var = if needTerm == x then (renameFreeVariables lst var) else (Variable x) 
  

-- Функция на один шаг в нормализации терма
eval1 :: Term -> Term
eval1 (Apply (Variable var) t2) = (Apply (Variable var)(eval1 t2))
eval1 (Apply (Lambda var res) t2) = setVariable [] res var t2
eval1 (Apply (Apply term1 term2) t2) = Apply (eval1 (Apply term1 term2)) t2
eval1 term = term


-- Функция на максимальное число шагов в нормализации терма
eval :: Term -> Term
eval (Apply (Variable var) t2) = (Apply (Variable var) (eval t2))
eval (Apply (Lambda var res) t2) = eval (setVariable [] res var t2)
eval (Apply (Apply term1 term2) t2) = eval (Apply (eval (Apply term1 term2)) t2)
eval term = term