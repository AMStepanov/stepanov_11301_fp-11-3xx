{-==== Степанов Александр, 11-301 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}

{- поиск ступени с максимальным отношением тяги к массе -}
maxPower :: [(Integer, Integer)] -> (Integer, Integer)
maxPower (x:[]) = x
maxPower (x:xs) = if ((fst x) `quot` (snd x)) > (fst (maxPower xs)) `quot` (snd (maxPower xs)) -- `quot` целочисленное деление div
                  then x
                  else maxPower xs
 
{- поиск ступени с минимальным весом -}
minWeight :: [(Integer, Integer)] -> Integer
minWeight (x:[]) = snd x
minWeight (x:xs) | snd x < minWeight xs = snd x
                 | otherwise = minWeight xs
 
{- если масса ступени равна минимальной - вернуть ее, иначе вернуть исходный список -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if snd (maxPower xs) == minWeight xs 
            then [maxPower xs] 
            else xs  

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}

getOrbiters :: [Load a] -> [Load a]
getOrbiters [] = []
getOrbiters ((Orbiter x):xs) = (Orbiter x):(getOrbiters xs)
getOrbiters ((Probe x):xs) = getOrbiters xs
 
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (getOrbiters x)
orbiters ((Rocket _ x):xs) = (orbiters xs) ++ (orbiters x)

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier ([Warp a]) = ["Kirk"] 
finalFrontier ([BeamUp s]) = ["Kirk"] 
finalFrontier ([IsDead s]) = ["McCoy"] 
finalFrontier ([LiveLongAndProsper]) = ["Spock"] 
finalFrontier ([Fascinating]) = ["Spock"] 

finalFrontier ((Warp a):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((BeamUp s):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((IsDead s):as) = ["McCoy"] ++ finalFrontier as 
finalFrontier ((LiveLongAndProsper):as) = ["Spock"] ++ finalFrontier as
finalFrontier ((Fascinating):as) = ["Spock"] ++ finalFrontier as
