
data List a = Nil
            | Cons a (List a)
            deriving (Eq,Show)

-- объявление класса типов
class Eq' a where
  -- функции для этого класса типов
  equals :: a -> a -> Bool
  notEquals :: a -> a -> Bool
  -- -- определение функций по умолчанию
  equals x y = not $ notEquals x y
  notEquals x y = not $ equals x y

data SomeType = Some Int
              | Type Bool

-- SomeType принадлежит классу типов Eq'
instance Eq' SomeType where
  -- как работает equals для значений SomeType
  equals (Some x) (Some y) = x==y
  equals (Type x) (Type y) = x==y
  equals _ _ = False
  -- можно не писать из-за реализации по умолчанию
  -- notEquals x y = not $ equals x y

instance (Eq' a) => Eq' (List a) where
  equals Nil Nil = True
  equals (Cons a as) (Cons b bs) = equals a b && equals as bs
  equals _ _ = False

-- НЕЛЬЗЯ, несколько раз объявляется equals:
-- class B a where
--   equals :: a -> a -> Bool
-- или
-- equals :: a -> a -> Bool
-- equals x y = x==y

class Eq' a => Ord' a where
  compare' :: a -> a -> Int

data A = A Int
       | B

instance Ord' A where
  compare' (A x) (A y) = x - y
  compare' B B = 0
  compare' (A _) B = -1
  compare' B (A _) = 1

instance Eq' A where
  equals (A x) (A y) = x==y
  equals B B = True
  equals _ _ = False

instance Eq A where
  (A x) == B = True
  _ == _ = False

instance Ord a => Ord (List a) where
  compare Nil Nil = EQ
  compare Nil (Cons _ _) = LT
  compare (Cons _ _) Nil = GT
  compare (Cons a as) (Cons b bs) = case compare a b of
    LT -> LT
    EQ -> compare as bs
    GT -> GT

{-
*Main> compare (Cons 5 (Cons 4 Nil)) (Cons 5 (Cons 5 Nil))
LT
*Main> compare (Cons 5 (Cons 4 Nil)) (Cons 5 (Cons 3 Nil))
GT
*Main> compare (Cons 5 (Cons 4 Nil)) (Cons 5 (Cons 4 Nil))
EQ
*Main> compare (Cons 5 (Cons 4 Nil)) (Cons 5 Nil)
GT
-}

-------------------------------------------------------------

data Set a = Set (a -> Bool)

member :: a -> Set a -> Bool
member a (Set f) = f a

empty :: Set a
empty = Set $ \_ -> False

singleton :: Eq a => a -> Set a
singleton a = Set $ \x -> x==a

insert :: Eq a => a -> Set a -> Set a
insert a (Set f) = Set $ \x -> f x || x==a

delete :: Eq a => a -> Set a -> Set a
delete a (Set f) = Set $ \x -> f x && x/=a

intersection :: Set a -> Set a -> Set a
intersection (Set f) (Set g) = Set $ \x -> f x && g x

union :: Set a -> Set a -> Set a
union (Set f) (Set g) = Set $ \x -> f x || g x

----------------------------------------------

-- Чёрчевы булевские константы
tru = \t -> (\f -> t)
fls = \t f -> f

-- проверка в чёрчевой кодировке
iff = \b thn els -> b thn els

-- перевод чёрчевой кодировки в булевский тип
toBool b = b True False

-- И в чёрчевой кодировке
and' = \b1 b2 -> \t f -> b1 (b2 t f) f

-- ИЛИ в чёрчевой кодировке
or' = \b1 b2 t f -> b1 t (b2 t f)

{-
*Main> toBool $ and' tru fls
False
*Main> toBool $ and' fls fls
False
*Main> toBool $ and' fls tru
False
*Main> toBool $ and' tru tru
True
*Main> toBool $ or' tru tru
True
*Main> toBool $ or' tru fls
True
*Main> toBool $ or' fls fls
False
*Main> toBool $ or' fls tru
True
*Main>
-}

-- Чёрчевы числа
zero = \s z -> z
one  = \s z -> s z
two  = \s z -> s (s z)
three= \s z -> s (s (s z))

ten  = \s z -> s (s (s (s (s  (s (s (s (s (s z)))))))))

-- Перевод чёрчевых чисел в обычные
toInt n = n (+1) 0

-- +1
inc = \n -> \s z -> s (n s z)
-- +
plus = \n m -> \s z -> n s (m s z)
-- *
mult = \n m -> \s z -> n (m s) z

